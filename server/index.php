<?
$time_start = microtime(true);

session_start();
error_reporting(E_ALL ^ E_NOTICE);

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/common/Init.php';

date_default_timezone_set($config['site_timezone']);

ini_set('default_charset',			$config['default_charset']);
ini_set('session.cookie_domain',	$config['origin_domain']);
ini_set('display_errors',			1);

$core = new Init();

// Устанавливаем конфигурацию
foreach($config as $key=>$value) $core->config->$key = $value;

// TODO
//$login = $core->user->tokenCheck($_COOKIE['AUTH_TOKEN']);
//
//if(!array_key_exists('error', $login)){
//    $token = $login['id'].':'.$login['token'];
//    setcookie('AUTH_TOKEN', $token, time()+86400, '/', $core->config->origin_domain);
//} else {
//    echo 'error';
//}

header("Content-Type: text/html; charset=" . $config['default_charset']);

// Загружаем плагин
if(@!$_GET || @$_GET['plugin']){

    $plugin_name = (isset($_GET['plugin'])) ? $_GET['plugin'] :$core->config->default_plugin;
    $plugin_name = strtolower($plugin_name);

    $core->plugin->name = $plugin_name;

    if(!$plugin_name){
        die(1);
    }else{

        $plugin_dir = __DIR__ . '/plugins/' . $plugin_name . '/';

        if(!is_dir($plugin_dir) || !is_file($plugin_dir . 'init.php')){
            die(2);
        }else{
            if(!is_file($plugin_dir . 'config.php')){
                die(3);
            }else{

                // Конфиг плагина
                require_once $plugin_dir . 'config.php';

                $core->plugin->info = (object) $plugin;
                $_action = isset($_GET['action']) ? $_GET['action'] : $plugin['default_action'];

                // Инициализатор плагина
                require_once $plugin_dir . 'init.php';

            }
        }
    }
}

if(@$_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest')
{
    print "\n\n<!--\r\n";
    $time_end = microtime(true);
    $exec_time = $time_end-$time_start;

    if(function_exists('memory_get_peak_usage'))
        print "memory peak usage: ".memory_get_peak_usage()." bytes\r\n";
    print "page generation time: ".$exec_time." seconds\r\n";
    print "-->";
}