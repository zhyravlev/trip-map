<?
$config = array(
    'default_plugin'   => 'home',
    'default_charset'   => 'utf-8',
    'origin_domain'     => 'map.mediaflystudio.ru',

    'db_server'         => 'localhost',
    'db_user'           => 'map',
    'db_password'       => '0R3q3D6x',
    'db_name'           => 'map',

    'nickname_re'       => "/^[A-Za-z]+[A-Za-z0-9]*(?:[_]{0,3}[A-Za-z0-9]+){2}$/",
    'email_re'          => "/^([a-z0-9_\.\+-]+\@[\da-z\.-]+\.[a-z\.]{2,6})$/",
    'full_name_re'      => "/(.*)/",
    'md5_re'            => "/^[a-f0-9]+$/"
);