<? include 'templates/head.php'; ?>
<body class="notAuth">

<div class="notAuth__headerBlueBg"></div>
<div class="notAuth__content">
    <div class="notAuth__content-header">Logotype</div>
    <form class="notAuth__content__form" id="formSignIn">

        <div class="notAuth__content__form-header">
            <ul>
                <li><a class="active" href="/signin/">Sign in</a></li>
                <li><a href="/signup/">Sign up for free!</a></li>
            </ul>
        </div>

        <div class="alert alert-danger" role="alert" style="display: none;">...</div>

        <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" name="email" class="form-control" id="email" placeholder="Email">
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
        </div>

        <button type="button" onclick="auth.signIn(this, '#formSignIn');" class="btn btn-primary btn__width-100" data-loading-text="Loading..." data-autocomplete="off">Submit</button>

    </form>
</div>
<? include 'templates/scripts.php'; ?>
</body>
</html>