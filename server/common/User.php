<?

require_once('Init.php');

class User extends Init
{

    private $table = 'users';

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
    }

    private function emailIsExist($email)
    {
        $query = $this->db->placehold("SELECT id FROM {$this->table} WHERE `email` = '{$email}'");
        $this->db->query($query);

        if ($this->db->num_rows() == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function create($object)
    {
        if (count($object) > 0) {

            if (!preg_match($this->config->email_re, $object['email'])) {
                return array('error' => 1);
            }

            if($this->emailIsExist($object['email'])){
                return array('error' => 2);
            }

            $query = $this->db->placehold("INSERT INTO {$this->table} SET `email`=?, `password`=?, `create_time`=?", $object['email'], md5($object['password']), time());

            if ($result = $this->db->query($query)) {

                $id = $this->db->insert_id();

                $token = $this->authByID($id);

                return array('token' => $token);
            } else {
                return array('error' => 3);
            }
        }
    }

    private function authByID($id)
    {
        $token = md5((rand(1111, 9999) * time()) - rand(11111, 99999));

        $query = $this->db->placehold("UPDATE {$this->table} SET `token`=? WHERE `id`=?", $token, $id);

        if ($this->db->query($query)) {
            return $id.':'.$token;
        } else {
            return array('error' => 4);
        }
    }

    public function auth($object)
    {
        if (count($object) > 0) {

            if (!preg_match($this->config->email_re, $object['email'])) {
                return array('error' => 1);
            }

            if (!$object['password']) {
                return array('error' => 5);
            }

            $query = $this->db->placehold("SELECT `id` FROM {$this->table} WHERE email=? AND password=?", $object['email'], md5($object['password']));

            $this->db->query($query);

            if ($this->db->num_rows() !== 1) {
                return array('error' => 12);
            } else {

                $result = $this->db->result();
                $token = md5((rand(1111, 9999) * time()) - rand(11111, 99999));

                $query = $this->db->placehold("UPDATE {$this->table} SET token=? WHERE email=? AND password=?", $token, $object['email'], md5($object['password']));

                if ($this->db->query($query)) {
                    return array(
                        'id' => $result->id,
                        'token' => $result->id.':'.$token
                    );
                } else {
                    return array('error' => 13);
                }

            }
        }
    }

    // TODO
    public function tokenCheck($token)
    {
        if ($token) {

            $separate = explode(':', $token);
            $id = $separate[0];
            $token = $separate[1];

            if (!is_numeric($id)){
                return array('error' => 14);
            }

            if (!preg_match($this->config->md5_re, $token)) {
                return array('error' => 15);
            }

            $query = $this->db->placehold("SELECT `id` FROM {$this->table} WHERE id=? AND token=?", $id, $token);

            $this->db->query($query);

            if ($this->db->num_rows() !== 1) {
                return array('error' => 16);
            } else {

                $result = $this->db->result();
                $refresh = $this->tokenRefresh($id);

                return array(
                    'id' => $result->id,
                    'token' => $refresh['token']
                );
            }
        }
    }

    // TODO
    private function tokenRefresh($id){

        $token = md5((rand(1111, 9999) * time()) - rand(11111, 99999));

        $query = $this->db->placehold("UPDATE {$this->table} SET `token`=? WHERE `id`=?", $token, $id);

        if ($this->db->query($query)) {
            return array(
                'token' => $token
            );
        } else {
            return array('error' => 17);
        }
    }
}