<?php

require_once('Init.php');

class Config
{

	private $vars = array();
	
	// В конструкторе записываем настройки файла в переменные этого класса
	// для удобного доступа к ним. Например: $simpla->config->db_user
	public function __construct(){}

	// Магическим методов возвращаем нужную переменную
	public function &__get($name)
	{
		if(isset($this->vars[$name]))
			return $this->vars[$name];
		else
			return NULL;
	}
	
	// Магическим методов задаём нужную переменную
	public function __set($name, $value)
	{
		$this->vars[$name] = $value;
	}	
}