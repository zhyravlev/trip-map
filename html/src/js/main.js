$(function () {

  /* jQuery ajax settings */

  $.ajaxSetup({
    cache: false,
    dataType: 'json'
  });

  /* --- */

  var menu = $('.menu'),
    menu_substrate = $('.menu__substrate'),
    toggle_menu = $('#toggle-menu');

  var toggle_menu_fn = function () {
    toggle_menu.toggleClass('in');
    menu.toggleClass('in')
  };

  toggle_menu.on('click', function () {
    toggle_menu_fn()
  });

  menu_substrate.on('click', function () {
    toggle_menu_fn()
  })
});

if (!Modernizr.geolocation) {
  alert('Geolocation API not supported!');
}

/* Map */

var _map = {

  layers: {
    osm: new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
      detectRetina: true
    }),
    map_box: new L.TileLayer('http://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      attribution: 'Imagery from <a href="http://mapbox.com/about/maps/">MapBox</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      subdomains: 'abcd',
      id: '<your id>',
      accessToken: '<your accessToken>'
    })
  },

  ui: {
    btn__getCurrentPosition: $('#btn__get-current-position')
  },

  me: {
    marker: null,
    circle: null
  },

  mapObject: {},

  create: function () {

    _map.mapObject = new L.Map('map', {
      center: [51.505, -0.09],
      zoom: 10,
      zoomControl: true
    });

    _map.mapObject.addLayer(_map.layers.osm); // default

    // other layers
    L.control.layers({
      'OSM': _map.layers.osm
    }, {}).addTo(_map.mapObject);

    if (window.location.pathname.indexOf('location') !== -1) {
      // Определяем местоположение.
      _map.getCurrentPosition();

      // Определили местоположение.
      _map.mapObject.on('locationfound', _map.locationOnFound);

      // Если произошла ошибка.
      _map.mapObject.on('locationerror', _map.locationOnError)
    }

    if (window.location.pathname.indexOf('map') !== -1) {

      var markers = [],
        bounds = [];

      var data = [
        {
          "location": [39.61, -105.02],
          "desc": {
            "name": "Anton"
          }
        },
        {
          "location": [39.74, -104.99],
          "desc": {
            "name": "Вася"
          }
        },
        {
          "location": [1.74, 0.99],
          "desc": {
            "name": "Ети"
          }
        }
      ];

      $.each(data, function (a, b) {
        markers.push(L.marker(b.location).bindPopup(b.desc.name));
        bounds.push(b.location)
      });

      L.layerGroup(markers).addTo(_map.mapObject);
      _map.mapObject.fitBounds(bounds, {padding: L.point(100, 100)})
    }
  },

  getCurrentPosition: function () {
    console.info('Start...');
    _map.ui.btn__getCurrentPosition.button('loading');
    _map.mapObject.locate({setView: true, maxZoom: 16})
  },

  locationOnFound: function (e) {

    console.info('Location', e);

    var radius = e.accuracy / 2;

    if (_map.me.marker === null && _map.me.circle === null) {
      _map.me.marker = L.marker(e.latlng).addTo(_map.mapObject);
      _map.me.circle = L.circle(e.latlng, radius, {
        weight: 1
      }).addTo(_map.mapObject)
    } else {
      _map.me.marker.setLatLng(e.latlng);
      _map.me.circle.setLatLng(e.latlng);
      _map.me.circle.setRadius(radius)
    }

    _map.ui.btn__getCurrentPosition.button('reset')
  },

  locationOnError: function (e) {
    console.error(e.message);
    _map.ui.btn__getCurrentPosition.button('reset')
  }
};

// Создаем карту.
//_map.create();

var config = {
  URL: {
    SIGN_UP: '?action=xhr&fn=create',
    SIGN_IN: '?action=xhr&fn=auth',
    DISCOVER: '/discover/'
  }
};

var lang = {
  RU: {
    ERROR_EMAIL_BAD_FORMAT: 'ERROR_EMAIL_BAD_FORMAT',
    ERROR_USER_ALREADY_EXISTS: 'ERROR_USER_ALREADY_EXISTS',
    ERROR_UNDEFINED: 'ERROR_UNDEFINED',
    ERROR_ENTER_EMAIL: 'ERROR_ENTER_EMAIL',
    ERROR_ENTER_PASSWORD: 'ERROR_ENTER_PASSWORD',
    ERROR_USER_NOT_FOUND: 'ERROR_USER_NOT_FOUND'
  }
};

var auth = {
  signUp: function(button_self, form_id){

    var button = $(button_self),
      form = $(form_id),
      email = form.find('#email'),
      password = form.find('#password'),
      alert = $('.alert.alert-danger');

    alert.hide();

    if(!email.val()){
      email.focus();
      alert.text(lang.RU.ERROR_ENTER_EMAIL).show()
    } else if(!password.val()){
      password.focus();
      alert.text(lang.RU.ERROR_ENTER_PASSWORD).show()
    } else {
      button.button('loading');

      var data_to_send = $(form).serialize();

      $.ajax({
        url: config.URL.SIGN_UP,
        data: data_to_send,
        method: 'POST',
        success: function(res){
          if(res.token){
            $.cookie('AUTH_TOKEN', res.token, { expires: 7, path: '/' });
            window.location.href = config.URL.DISCOVER
          } else {
            switch(res.error){
              case 1: alert.text(lang.RU.ERROR_EMAIL_BAD_FORMAT).show(); break;
              case 2: alert.text(lang.RU.ERROR_USER_ALREADY_EXISTS).show(); break;
              default: alert.text(lang.RU.ERROR_UNDEFINED).show()
            }
          }

          button.button('reset')
        }
      })
    }
  },

  signIn: function(button_self, form_id){

    var button = $(button_self),
      form = $(form_id),
      email = form.find('#email'),
      password = form.find('#password'),
      alert = $('.alert.alert-danger');

    alert.hide();

    if(!email.val()){
      email.focus();
      alert.text(lang.RU.ERROR_ENTER_EMAIL).show()
    } else if(!password.val()){
      password.focus();
      alert.text(lang.RU.ERROR_ENTER_PASSWORD).show()
    } else {
      button.button('loading');

      var data_to_send = $(form).serialize();

      $.ajax({
        url: config.URL.SIGN_IN,
        data: data_to_send,
        method: 'POST',
        success: function(res){
          if(res.token){
            $.cookie('AUTH_TOKEN', res.token, { expires: 7, path: '/' });
            window.location.href = config.URL.DISCOVER
          } else {
            switch(res.error){
              case 1: alert.text(lang.RU.ERROR_EMAIL_BAD_FORMAT).show(); break;
              case 5: alert.text(lang.RU.ERROR_ENTER_PASSWORD).show(); break;
              case 12: alert.text(lang.RU.ERROR_USER_NOT_FOUND).show(); break;
              default: alert.text(lang.RU.ERROR_UNDEFINED).show()
            }
          }

          button.button('reset')
        }
      })
    }
  }
};