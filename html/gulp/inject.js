'use strict';

var path = require('path');
var gulp = require('gulp');
var wiredep = require('wiredep').stream;
var browserSync = require('browser-sync');
var $ = require('gulp-load-plugins')();

var conf = require('./conf');

gulp.task('inject-reload', ['inject'], function () {
  browserSync.reload();
});

// Инжектим в *.html .css и .js файлы нашего проекта после того как они будут скомпилированы

gulp.task('inject', ['styles', 'scripts', 'fonts'], function () {

  var injectTo = path.join(conf.paths.src, '/*.html');  // читаем файлы ./src/*.html

  var injectFiles = gulp.src(
    [
      path.join(conf.paths.tmp, '/**/*.js'),
      path.join(conf.paths.tmp, '/**/*.css')
    ],
    {
      read: false
    }
  );

  // путь будет не /.tmp/css ... а css/
  var injectOptions = {
    /*ignorePath: [
     conf.paths.tmp
     ],
     addRootSlash: false*/
  };

  var wiredepOptions = {
    exclude: ['bootstrap-sass'], // не инжектит файлы bootstrap-sass
    ignorePath: /^(\.\.\/)*\.\./ // пусть будет не ../bower_components/ ... а /bower_components/ ...
  };

  return gulp.src(injectTo)
    .pipe($.inject(injectFiles, injectOptions))
    .pipe(wiredep(wiredepOptions))
    .pipe(gulp.dest(conf.paths.src)); // сохраняем в ./tmp
});